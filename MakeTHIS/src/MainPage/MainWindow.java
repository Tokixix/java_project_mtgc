package MainPage;
import StartPage.*;
import DrawingPart.actual;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import DrawingPart.DrawingCard;
import PanelMenu.MainFunctions;
@SuppressWarnings("unused")
public class MainWindow extends JFrame{
		StartWindow START;
		DrawingCard drawingcard;
		actual a = new actual();
		public Color CL[] = new Color[5];
		public MainWindow(StartWindow START,Color CL[])
		{
		    super(Info.defaultWindowName);
		    this.START = START;
		    this.CL = CL;
		  //  System.out.println(this.CL[0]);
		    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            setSize(Info.defaultWindowSize);
            setVisible(true);
            setLayout(new FlowLayout());
            setLocationRelativeTo(null);
            drawingcard = new DrawingCard(START.MAIN_IMAGE,this.CL, a);
            setJMenuBar(new MyMenu(drawingcard));
            add(drawingcard);
            add(new MainFunctions(drawingcard, this.CL, a), BorderLayout.CENTER);
            setJMenuBar(new MyMenu(drawingcard));
	    }

}
