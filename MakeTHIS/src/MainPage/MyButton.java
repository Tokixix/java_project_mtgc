package MainPage;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
public class MyButton extends JButton{
	MyButton(String S){
		super(S);
        this.setFont( new Font("Dialog", Font.BOLD | Font.ITALIC, 10));
        this.setForeground(Color.RED);
	}
}
