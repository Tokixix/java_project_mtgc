package MainPage;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;

import DrawingPart.DrawingCard;

import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Mac; // menu glowne czyli pasek na samej gorze umorzliwiajacy zapis itd
public class MyMenu extends JMenuBar
{
	DrawingCard drawing;
	public MyMenu(DrawingCard drawing)
	{
		this.drawing = drawing;
		JMenu fileMenu = new JMenu("File");
		this.add(fileMenu);
		JMenu helpMenu = new JMenu("Help");
		this.add(helpMenu);
		JMenuItem openItem = new JMenuItem("Open"); 
		fileMenu.add(openItem); 
		JMenuItem saveItem = new JMenuItem("Save"); 
		Action saveAction = new AbstractAction("zapisz") 
		{
			public void actionPerformed(ActionEvent event) 
			{
				BufferedImage bim = new BufferedImage(drawing.getWidth(), drawing.getHeight(), BufferedImage.TYPE_INT_RGB);
				drawing.paintAll(bim.getGraphics());
		        try {
		            ImageIO.write(bim, "jpg", new File("panel.jpg"));
		        } catch (IOException ex) { 		            //Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
		        }
			}
		};
		saveItem.addActionListener(saveAction);
		fileMenu.add(saveItem); 
		fileMenu.addSeparator();
	}
	
}
