package MainPage;

import java.awt.Dimension;

public class Info {
	static final String defaultWindowName = "Make THIS";
	static final int defaultWindowWidth  = 1200;
	static final int defaultWindowHeight = 900;
	static Dimension defaultWindowSize = 
					new Dimension(defaultWindowWidth, defaultWindowHeight);
}
