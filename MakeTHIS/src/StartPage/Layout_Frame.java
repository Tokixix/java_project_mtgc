package StartPage;

import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.imageio.ImageIO;
import javax.swing.*;

public class Layout_Frame extends JFrame implements ActionListener{
	JButton[] B;
	Image[] Img;
	StartWindow S_W;
	public Layout_Frame(StartWindow S_W) {
		super("LAYOUTS");
		B = new JButton[9];
		Img = new Image[9];
		for(int i = 1; i <= 9; i++){
			Img[i - 1] =  new ImageIcon(this.getClass().getResource("/"+ new Integer(i).toString() + ".jpg")).getImage();
		}
		for(int i = 0; i < 9; i++){
			B[i] = new JButton();
		}
		this.S_W = S_W;
		this.setSize(600,600);
		this.setVisible(true);
		this.setLayout(new GridLayout());
		setLocationRelativeTo(null);
		for(int i = 0; i < 9; i++){
			B[i].setIcon(new ImageIcon(Img[i]));
		}
		for(int i = 0; i < 9; i++){
			B[i].addActionListener(this); 
			add(B[i]);
		}
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		for(int i = 0; i < 9; i++){
			if(e.getSource() == B[i]){
				S_W.Layout.setIcon(new ImageIcon(Img[i]));
				S_W.MAIN_IMAGE = Img[i];
				this.dispose();
			}
		}
	}
}
