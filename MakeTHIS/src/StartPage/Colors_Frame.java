package StartPage;
import java.awt.*;
import java.awt.event.WindowEvent;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Colors_Frame extends JFrame implements ChangeListener{
	JButton Colors;
	JColorChooser JCC;
	StartWindow S_W;
	static int i;
	Colors_Frame(StartWindow S_W, JButton Colors){
		super("COLORS");
		this.S_W = S_W;
		this.Colors = Colors;
		this.setSize(600,600);
		this.setVisible(true);
		setLocationRelativeTo(null);
		JCC = new JColorChooser();
		add(JCC);
		JCC.getSelectionModel().addChangeListener(this);
	//	S_W.CL[0] = Color.RED;
	//	S_W.CL[1] = Color.YELLOW;
	//	S_W.CL[2] = Color.GREEN;
	//	S_W.CL[3] = Color.BLUE;
	//	S_W.CL[4] = Color.BLACK;
			
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		Color newColor = JCC.getColor();
		S_W.CL[i++] = newColor;
		if (i == 5)
		 this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
		//Colors.setBackground(newColor);
	}
}
