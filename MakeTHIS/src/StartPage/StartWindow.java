package StartPage;
import MainPage.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
// OKNO OTWIERAJACE APLIKACJI
public class StartWindow extends JFrame implements ActionListener{
	JButton Start = new JButton("Start");
	JButton Layout = new JButton("Layout");
	JButton Colors = new JButton("Colors");
	//public Color MAIN_COLOR;
	public Image MAIN_IMAGE;
	public Color[] CL = new Color[5];
	StartWindow(){        //KONSTRUKTOR TWORZACY PRZYCISKI UWIDOCZNIAJCY OKNO
		super("MakeTHIS!");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(1000,1000);
		this.setVisible(true);
		this.setBackground(new Color(100,32,11));
		setLocationRelativeTo(null);
		GridLayout Lay = new GridLayout();
		setLayout(Lay);
		add(Layout);
		add(Start);
		add(Colors);
		Start.addActionListener(this);
		Layout.addActionListener(this);
		Colors.addActionListener(this);
		Start.setVisible(true);
		Layout.setVisible(true);
		Colors.setVisible(true);
		CL[0] = Color.RED;
		CL[1] = Color.YELLOW;
		CL[2] = Color.GREEN;
		CL[3] = Color.BLUE;
		CL[4] = Color.BLACK;
	}
	public void actionPerformed(ActionEvent e) {  //FUNKCJONALNOSCI PRZYCISKOW
		if(e.getSource() == Start){
			MainWindow MAIN = new MainWindow(this,CL);
      	  	this.dispose();
		}
		if(e.getSource() == Layout){
			Layout_Frame LF = new Layout_Frame(this);
		}
		if(e.getSource() == Colors){
			Colors_Frame CF = new Colors_Frame(this,Colors);
		}
	}
		
}

