package PanelMenu;
import javax.swing.*;
import DrawingPart.*;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
// panel glowny zawierajacy trzy zakladki
public class MainFunctions extends JPanel
{
     DrawingCard mypanel;
     public actual actual;
     public Color CL[] = new Color[5];
	 public MainFunctions(DrawingCard mypanel, Color CL[], actual a)
     {
    	 super(new BorderLayout());
    	 this.actual = a;
    	 this.mypanel = mypanel;
    	 this.CL = CL;
    	 JTabbedPane tabbedPane = new JTabbedPane();
    	 JComponent panel1 = new ObjectsPanel(mypanel, actual);
    	 tabbedPane.add("Objects", panel1);
    	 tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);

    	 JComponent panel2 = new ColorPanel(mypanel, CL, actual);
    	 tabbedPane.addTab("Colors", panel2);
    	 tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);

    	 JComponent panel3 = new FunctionPanel(mypanel);
    	 tabbedPane.addTab("Functions", panel3);
    	 tabbedPane.setMnemonicAt(2, KeyEvent.VK_3);
    	 
    	 JComponent panel4 = new TextPanel(mypanel);
    	 tabbedPane.addTab("Text", panel4);
    	 tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);
    	 
    	 tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
    	 add(tabbedPane);
    	 
    	 
     }
     protected JComponent makeTextPanel(String text) {
         JPanel panel = new JPanel(false);
         JLabel filler = new JLabel(text);
         filler.setHorizontalAlignment(JLabel.CENTER);
         panel.setLayout(new GridLayout(1, 1));
         panel.add(filler);
         return panel;
     }
}
