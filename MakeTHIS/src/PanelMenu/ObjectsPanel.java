package PanelMenu;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import DrawingPart.*;
import javax.swing.*;
public class ObjectsPanel extends JPanel implements ActionListener, PANEL//panel zawierajacy przyciski ksztalty ktorych przycisniecie powoduje pojawienie sie ich na ekranie
{
	private static final int DEFAULT_WIDTH = 1200;
	private static final int DEFAULT_HEIGHT = 200;
	private JButton circleButton;
	private JButton squareButton;
	private JButton triangleButton;
	private JButton rectangleButton;
	public DrawingCard mypanel;
	public actual a;
	public ObjectsPanel(DrawingCard mypanel, actual a) //konstruktor tworzy poszczeglne guziki
	{
		this.mypanel = mypanel;
		this.a = a;
		circleButton = new MyButtonObj("kolo", mypanel,new ElipseMaker());
		add(circleButton);
		circleButton.addActionListener(this);
		squareButton = new MyButtonObj("kwadrat",mypanel, new RectangleMaker());
		add(squareButton);
		squareButton.addActionListener(this);
		triangleButton = new MyButtonObj("kwadrat2",mypanel, new RoundRectangleMaker());
		triangleButton.addActionListener(this);
		add(triangleButton);
		rectangleButton = new MyButtonObj("prostokat", mypanel, new ElipseMaker());
		add(rectangleButton);
		rectangleButton.addActionListener(this);
	}
	public Dimension getPreferredSize()     //ustaw rozmiar
    { 
	    return new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT); 
    }
	public void actionPerformed(ActionEvent e) {
		MyButtonObj Caller = (MyButtonObj)e.getSource();
		SMI SM = Caller.SM;
		mypanel.setShapeMaker(SM);
	}
}
