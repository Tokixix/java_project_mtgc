package PanelMenu;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;

import javax.swing.JButton;
import javax.swing.JPanel;

import DrawingPart.DrawingCard;
import DrawingPart.ElipseMaker;
import DrawingPart.RectangleMaker;
import DrawingPart.RoundRectangleMaker;
import DrawingPart.ShapeMaker;

public class TextPanel extends JPanel implements ActionListener, PANEL{
	private static final int DEFAULT_WIDTH = 1200;
	private static final int DEFAULT_HEIGHT = 200;
	private JButton fontButton;
	private JButton sizeButton;
	private JButton colorButton;
	private JButton insertButton;
	public DrawingCard mypanel;
	public TextPanel(DrawingCard mypanel) 
	{
		this.mypanel = mypanel;
		fontButton = new FunctionButton("Font");
		add(fontButton);
		fontButton.addActionListener(this);
		
		sizeButton = new FunctionButton("Size");
		add(sizeButton);
		sizeButton.addActionListener(this);
		
		colorButton = new FunctionButton("Color");
		add(colorButton);
		colorButton.addActionListener(this);
		
		insertButton = new FunctionButton("Insert");
		add(insertButton);
		insertButton.addActionListener(this);
	}
	public Dimension getPreferredSize()
    { 
	    return new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT); 
    }
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == fontButton){
			
		}
        if(e.getSource() == sizeButton){
			
		}
        if(e.getSource() == colorButton){
			
		}
        if(e.getSource() == insertButton){
			
		}
	}
}