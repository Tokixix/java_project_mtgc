package PanelMenu;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;

import javax.swing.JButton;
import javax.swing.JPanel;

import DrawingPart.DrawingCard;
import DrawingPart.ElipseMaker;
import DrawingPart.RectangleMaker;
import DrawingPart.RoundRectangleMaker;
import DrawingPart.ShapeMaker;

public class FunctionPanel extends JPanel implements ActionListener, PANEL{
	private static final int DEFAULT_WIDTH = 1200;
	private static final int DEFAULT_HEIGHT = 200;
	private JButton rotateButton;
	private JButton clearButton;
	private JButton undoButton;
	public DrawingCard mypanel;
	public FunctionPanel(DrawingCard mypanel) 
	{
		this.mypanel = mypanel;
		rotateButton = new FunctionButton("Rotate");
		add(rotateButton);
		rotateButton.addActionListener(this);
		clearButton = new FunctionButton("Clear");
		add(clearButton);
		clearButton.addActionListener(this);
		undoButton = new FunctionButton("Undo");
		add(undoButton);
		undoButton.addActionListener(this);
	}
	public Dimension getPreferredSize()
    { 
	    return new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT); 
    }
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == rotateButton){
			mypanel.t = new AffineTransform(5,6,7,4,2,1);
			mypanel.repaint();
		}
		if(e.getSource() == clearButton){
			mypanel.L.clear();
			mypanel.repaint();
		}
		if(e.getSource() == undoButton){
			mypanel.L.removeLast();
			mypanel.repaint();
		}
	}
}
