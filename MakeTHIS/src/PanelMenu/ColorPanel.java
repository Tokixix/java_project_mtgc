package PanelMenu;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import DrawingPart.DrawingCard;
import DrawingPart.ElipseMaker;
import DrawingPart.RectangleMaker;
import DrawingPart.RoundRectangleMaker;
import DrawingPart.ShapeMaker;
import StartPage.Colors_Frame;
import DrawingPart.actual;
public class ColorPanel extends JPanel implements ActionListener, PANEL{
	private static final int DEFAULT_WIDTH = 1200;
	private static final int DEFAULT_HEIGHT = 200;
	private JButton customButton;
	private JButton ColorButton[] = new ColorButton[5];
	private Color CL[];
	JColorChooser JCC = new JColorChooser();
	//Chooser JCC;
	public DrawingCard mypanel;
	public ColorPanel(DrawingCard mypanel, Color CL[]) 
	{
		this.mypanel = mypanel;
		this.CL = CL;
		for(int i = 0; i < 5; i++){
			this.ColorButton[i] = new ColorButton(CL[i]);
			add(ColorButton[i]);
			ColorButton[i].addActionListener(this);
		}
		customButton = new FunctionButton("Custom");
		add(customButton);
		customButton.addActionListener(this);
	}
	public Dimension getPreferredSize()
    { 
	    return new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT); 
    }
	@Override
	public void actionPerformed(ActionEvent e) {
		Chooser chooser;
		if (e.getSource() == customButton)
	       chooser = new Chooser(mypanel);
	   if (e.getSource() == ColorButton[0])
			a.actualcolor = CL[0];
		else if (e.getSource() == ColorButton[1])
			a.actualcolor = CL[1];
		else if (e.getSource() == ColorButton[2])
			a.actualcolor = CL[2];
		else if (e.getSource() == ColorButton[3])
			a.actualcolor = CL[3];
		else if (e.getSource() == ColorButton[4])
			a.actualcolor = CL[4];
	}
}