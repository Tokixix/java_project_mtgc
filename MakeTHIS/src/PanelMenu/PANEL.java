package PanelMenu;

import java.awt.Dimension;
import java.awt.event.ActionEvent;

public interface PANEL {

	Dimension getPreferredSize();

	void actionPerformed(ActionEvent e);

}