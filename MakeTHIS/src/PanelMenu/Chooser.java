package PanelMenu;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import DrawingPart.DrawingCard;
import StartPage.StartWindow;

public class Chooser extends JFrame implements ChangeListener{
	JColorChooser JCC;
	DrawingCard mypanel;
	Chooser(DrawingCard mypanel){
		super("COLORS");
		this.mypanel = mypanel;
		this.setSize(600,600);
		this.setVisible(true);
		setLocationRelativeTo(null);
		JCC = new JColorChooser();
		add(JCC);
		JCC.getSelectionModel().addChangeListener(this);
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		Color newColor = JCC.getColor();
		//mypanel.C = newColor;
	}
}