package PanelMenu;

import java.awt.Color;

import javax.swing.JButton;

public class ColorButton extends JButton{
	ColorButton(Color C){
		super();
		this.setBackground(C);
		setOpaque(true);
		setBorderPainted(false);
	}
}
