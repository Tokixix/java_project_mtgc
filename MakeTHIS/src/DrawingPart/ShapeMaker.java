package DrawingPart;

import java.awt.Shape;
import java.awt.geom.Point2D;

public abstract class ShapeMaker implements SMI 
{
	/* (non-Javadoc)
	 * @see DrawingPart.SMI#makeShape(java.awt.geom.Point2D[])
	 */
	@Override
	public abstract Shape makeShape(Point2D[] p);
	private int pointCount;
	public ShapeMaker(int aPointCount) 	//Tworzy obiekt klasy ShapeMaker 	//aPointCount liczba punktów kontrolnych definiujących figurę
	{
	    pointCount = aPointCount; 
	}
	/* (non-Javadoc)
	 * @see DrawingPart.SMI#getPointCount()
	 */
	@Override
	public int getPointCount() 	// Zwraca liczbę punktów kontrolnych definiujących figurę 
	{
		return pointCount;
	}
	/* (non-Javadoc)
	 * @see DrawingPart.SMI#toString()
	 */
	@Override
	public String toString()  	// Tworzy figurę na podstawie zbioru punktów
	{
	    return getClass().getName();
	}
}
