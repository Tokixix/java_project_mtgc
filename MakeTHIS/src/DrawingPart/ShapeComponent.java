package DrawingPart;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;

import javax.swing.*;

//komponent rysujacy figure i umozliwiajacy jej przesuwanie
public class ShapeComponent extends JComponent
{
	private static final int DEFAULT_WIDTH = 300; 
	private static final int DEFAULT_HEIGHT = 200;
	private Point2D[] points;
	private static Random generator = new Random(); 
	private static int SIZE = 10;
	private int current;
	private SMI shapeMaker;
	private Image I;
	//private Color C = new Color(3,43,132);
	public actual a;
	public LinkedList<Shape> L = new LinkedList<Shape>();
	public AffineTransform t = new AffineTransform();
	private boolean Ifadd = false;
	ComponentObj test = new ComponentObj();
	public ShapeComponent(Image Bg, actual a) 
	{
		I = Bg;
		this.a = a;
		addMouseListener(new MouseAdapter() 
		{
	         public void mousePressed(MouseEvent event) 
	         {
	 			Point p;
	             p = event.getPoint();
	             try{
	             for (int i = 0; i < points.length; i++) 
	             {
	                 double x = points[i].getX() - SIZE / 2;
	                 double y = points[i].getY() - SIZE / 2;
	                 Rectangle2D r = new Rectangle2D.Double(x, y, SIZE, SIZE);
	                 if (r.contains(p))
	                 {
	                     current = i;
	                     return; 
	                 }
	              } 
	             }
	             catch(Exception e){
	             }
	           }
	           public void mouseReleased(MouseEvent event) 
	           {	
		           current = -1;
		           Ifadd = true;
			       repaint();
	           }
	      });
	addMouseMotionListener(new MouseMotionAdapter()
	{
	     public void mouseDragged(MouseEvent event) 
	     {
	         if (current == -1) 
	        	 return; 
	         points[current] = event.getPoint(); 
	      } 
	 });
	current = -1; 
	}
	public void setShapeMaker(SMI aShapeMaker) 
	{
		shapeMaker = aShapeMaker;
		int n = shapeMaker.getPointCount(); 
		points = new Point2D[n];
		for (int i = 0; i < n; i++)
		{
		    double x = generator.nextDouble() * getWidth(); 
		    double y = generator.nextDouble() * getHeight(); 
		    points[i] = new Point2D.Double(x, y);
		}
		repaint();
	 }
	public void paintComponent(Graphics g) 
	{
		Graphics2D g2 = (Graphics2D) g;
		g2.drawImage(I, 0, 0, this);
		g2.setColor(a.actualcolor);
		if (points == null) 
			return;
		for (int i = 0; i < points.length; i++) 
		{
		    double x = points[i].getX() - SIZE / 2;
		    double y = points[i].getY() - SIZE / 2; 
		    g2.fill(new Rectangle2D.Double(x, y, SIZE, SIZE));
		}
		Shape shape = shapeMaker.makeShape(points);
		System.out.println(Ifadd);
		if(Ifadd == true){
			L.add(shape);
		}
		for(Shape x : L){
			System.out.println(x);
			g2.fill(x);
			g2.draw(x);
		}		
	}
	public Dimension getPreferredSize() 
	{ 
		return new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT); 
	}
}
