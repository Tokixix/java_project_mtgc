package DrawingPart;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;

import javax.swing.JComponent;

public class ComponentObj extends JComponent{
	private static final int DEFAULT_WIDTH = 300; 
	private static final int DEFAULT_HEIGHT = 200;
	public ComponentObj(){
	}
	public void paintComponent(Graphics g) 
	{
		super.paintComponent(g);
		Graphics2D Graphics2D = (Graphics2D)g;	
		g.draw3DRect(500, 500, 500, 300, true);
	}
	public Dimension getPreferredSize() 
	{ 
		return new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT); 
	}
}
