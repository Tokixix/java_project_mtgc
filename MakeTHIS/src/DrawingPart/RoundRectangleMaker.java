package DrawingPart;

import java.awt.Shape;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;

public class RoundRectangleMaker extends ShapeMaker{
		public RoundRectangleMaker(){
			super(2);
		}

		@Override
		public Shape makeShape(Point2D[] p) {
			RoundRectangle2D s = new RoundRectangle2D.Double(0,0,0,0,40,40);
			s.setFrameFromDiagonal(p[0], p[1]);
			return s;
		}
}	
