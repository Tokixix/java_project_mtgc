package DrawingPart;

import java.awt.Shape;
import java.awt.geom.Point2D;

public interface SMI {

	Shape makeShape(Point2D[] p);

	int getPointCount();

	String toString();

}